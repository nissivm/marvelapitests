//
//  MarvelViewModel.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 13/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct MarvelViewModel {
    
    func getMarvelData<T: APIRequest>( _ request: T, completion: @escaping ResultCallback<DataContainer<T.Response>>) {
        MarvelAPIClient().send(request, completion: { response in
            DispatchQueue.main.async {
                switch response {
                    case .success(let data):
                        completion(.success(data))
                    case .failure(let error):
                        completion(.failure(error))
                }
            }
        })
    }
}
