//
//  Environments.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 12/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct Environments {
    
    let publicKey: String
    let privateKey: String
    let baseUrl: String
    
    init() {
        guard let infoDictionary = Bundle.main.infoDictionary else {
            fatalError("No Info.plist in project!")
        }
        
        guard let pubKey = infoDictionary["PUBLIC_KEY"] as? String else {
            fatalError("No public key!")
        }
        
        publicKey = pubKey
        
        guard let priKey = infoDictionary["PRIVATE_KEY"] as? String else {
            fatalError("No private key!")
        }
        
        privateKey = priKey
        
        guard let bUrl = infoDictionary["BASE_URL"] as? String else {
            fatalError("No base url!")
        }
        
        baseUrl = bUrl
    }
}
