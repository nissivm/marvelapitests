//
//  ViewController.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 09/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum List {
        case characters
        case comics
        case series
    }
    
    private var charactersArray = Array<ComicCharacter>()
    private var comicsArray = Array<ComicBook>()
    private var listInUse: List = .characters
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 50
    }
    
    // MARK: - IBActions
    
    @IBAction func charactersButtonTapped(_ sender: UIButton) {
        listInUse = .characters
        fetchData(GetCharacters(), completion: {
            [weak self] response in
            switch response {
            case .success(let data):
                self?.charactersArray = data.results
                self?.tableView.reloadData()
            case .failure(let error):
                print("Error \(error) fetching characters")
            }
        })
    }
    
    @IBAction func comicsButtonTapped(_ sender: UIButton) {
        listInUse = .comics
        fetchData(GetComics(), completion: {
            [weak self] response in
            switch response {
            case .success(let data):
                self?.comicsArray = data.results
                self?.tableView.reloadData()
            case .failure(let error):
                print("Error \(error) fetching characters")
            }
        })
    }
    
    @IBAction func seriesButtonTapped(_ sender: UIButton) {
    }
    
    // MARK: - Fetch data
    
    private func fetchData<T: APIRequest>( _ request: T, completion: @escaping ResultCallback<DataContainer<T.Response>>) {
        spinner.startAnimating()
        MarvelViewModel().getMarvelData(request, completion: {
            [weak self] response in
            self?.spinner.stopAnimating()
            switch response {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch listInUse {
            case .characters:
                return charactersArray.count
            case .comics:
                return comicsArray.count
            default:
                return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MarvelTableViewCell", for: indexPath) as? MarvelTableViewCell else {
            return UITableViewCell()
        }
        
        var title: String?
        var description: String?
        var thumbnail: Image?
        
        switch listInUse {
            case .characters:
                title = charactersArray[indexPath.row].name
                description = charactersArray[indexPath.row].description
                thumbnail = charactersArray[indexPath.row].thumbnail
            case .comics:
                title = comicsArray[indexPath.row].title
                description = comicsArray[indexPath.row].description
                thumbnail = comicsArray[indexPath.row].thumbnail
            default:
                title = ""
                description = ""
        }
        
        cell.thumbnail.load(url: thumbnail?.url)
        cell.title.text = title
        cell.descrip.text = description
        
        return cell
    }
}

