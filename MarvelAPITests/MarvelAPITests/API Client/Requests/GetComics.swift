//
//  GetComics.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 13/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

enum ComicFormat: String {
    case comic = "comic"
    case magazine = "magazine"
    case hardcover = "hardcover"
    case digest = "digest"
}

struct GetComics: APIRequest {
    
    typealias Response = [ComicBook]

    var endpoint: String {
        return "comics"
    }
    
    var parameters: Dictionary<String, String> {
        return params
    }
    
    private var params = Dictionary<String, String>()
    
    init(format: ComicFormat? = nil,
         title: String? = nil,
         titleStartsWith: String? = nil,
         startYear: Int? = nil,
         issueNumber: Int? = nil) {
        
        if let format = format {
            params["format"] = format.rawValue
        }
        if let title = title {
            params["title"] = title
        }
        if let titleStartsWith = titleStartsWith {
            params["titleStartsWith"] = titleStartsWith
        }
        if let startYear = startYear {
            params["startYear"] = "\(startYear)"
        }
        if let issueNumber = issueNumber {
            params["issueNumber"] = "\(issueNumber)"
        }
    }
}
