//
//  GetCharacters.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 09/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct GetCharacters: APIRequest {
    
    typealias Response = [ComicCharacter]

    var endpoint: String {
        return "characters"
    }
    
    var parameters: Dictionary<String, String> {
        return params
    }
    
    private var params = Dictionary<String, String>()

    init(name: String? = nil,
         nameStartsWith: String? = nil,
         limit: Int? = nil,
         offset: Int? = nil) {
        
        if let name = name {
            params["name"] = name
        }
        if let nameStartsWith = nameStartsWith {
            params["nameStartsWith"] = nameStartsWith
        }
        if let limit = limit {
            params["limit"] = "\(limit)"
        }
        if let offset = offset {
            params["offset"] = "\(offset)"
        }
    }
}
