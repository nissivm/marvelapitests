//
//  MarvelAPIClient.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 10/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public typealias ResultCallback<Value> = (Result<Value, Error>) -> Void

public class MarvelAPIClient {
    
    private let session = URLSession(configuration: .default)
    
    public func send<T: APIRequest>( _ request: T, completion: @escaping ResultCallback<DataContainer<T.Response>>) {
        
        guard let endpoint = self.endpoint(for: request) else {
            completion(.failure(MarvelError.invalidEndpoint))
            return
        }
        
        let task = session.dataTask(with: URLRequest(url: endpoint)) { data, response, error in
            if let data = data {
                do {
                    let marvelResponse = try JSONDecoder().decode(MarvelResponse<T.Response>.self, from: data)

                    if let dataContainer = marvelResponse.data {
                        completion(.success(dataContainer))
                    } else if let message = marvelResponse.message {
                        completion(.failure(MarvelError.server(message: message)))
                    } else {
                        completion(.failure(MarvelError.decoding))
                    }
                } catch {
                    completion(.failure(error))
                }
            } else if let error = error {
                completion(.failure(error))
            }
        }
        
        task.resume()
    }
    
    private func endpoint<T: APIRequest>(for request: T) -> URL? {
        
        guard let baseUrl = URL(string: AppDelegate.environments.baseUrl) else {
            print("Could not create base url from string: \(AppDelegate.environments.baseUrl)")
            return nil
        }
        
        guard let urlEndpoint = URL(string: request.endpoint, relativeTo: baseUrl) else {
            print("Bad endpoint name: \(request.endpoint)")
            return nil
        }
        
        guard var components = URLComponents(url: urlEndpoint, resolvingAgainstBaseURL: true) else {
            print("Could not create components for url: \(urlEndpoint)")
            return nil
        }
        
        var queryItems = [URLQueryItem]()
        
        if let requiredQueryItems = getRequiredQueryItems() {
            queryItems.append(contentsOf: requiredQueryItems)
        }
        
        if let optionalQueryItems = getOptionalQueryItems(for: request) {
            queryItems.append(contentsOf: optionalQueryItems)
        }
        
        components.queryItems = queryItems
        
        guard let finalUrl = components.url else {
            return nil
        }
        
        return finalUrl
    }
    
    // MARK: - Required query items needed for all Marvel requests
    
    private func getRequiredQueryItems() -> [URLQueryItem]? {
        
        let timestamp = "\(Date().timeIntervalSince1970)"
        let hash = "\(timestamp)\(AppDelegate.environments.privateKey)\(AppDelegate.environments.publicKey)".md5
        return [
            URLQueryItem(name: "ts", value: timestamp),
            URLQueryItem(name: "hash", value: hash),
            URLQueryItem(name: "apikey", value: AppDelegate.environments.publicKey)
        ]
    }
    
    // MARK: - Optional query items needed for this specific request
    
    private func getOptionalQueryItems<T: APIRequest>(for request: T) -> [URLQueryItem]? {
        
        guard request.parameters.count > 0 else {
            return nil
        }
        
        var queryItems = [URLQueryItem]()
        
        _ = request.parameters.map { key, value in
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        
        return queryItems
    }
}
