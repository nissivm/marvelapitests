//
//  APIRequest.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 09/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public protocol APIRequest: Encodable {
    associatedtype Response: Decodable
    var endpoint: String { get }
    var parameters: Dictionary<String, String> { get }
}
