//
//  ComicBook.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 13/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct ComicBook: Decodable {
    let id: Int
    let title: String?
    let description: String?
    let thumbnail: Image?
}
