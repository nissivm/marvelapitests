//
//  MarvelResponse.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 10/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct MarvelResponse<Response: Decodable>: Decodable {
    let status: String?
    let message: String?
    let data: DataContainer<Response>?
}
