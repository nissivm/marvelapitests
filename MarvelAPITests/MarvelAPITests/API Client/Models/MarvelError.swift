//
//  MarvelError.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 09/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public enum MarvelError: Error {
    case encoding
    case decoding
    case invalidEndpoint
    case server(message: String)
}
