//
//  DataContainer.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 10/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

public struct DataContainer<Results: Decodable>: Decodable {
    let offset: Int
    let limit: Int
    let total: Int
    let count: Int
    let results: Results // Varies from request to request
}
