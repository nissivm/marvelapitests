//
//  ComicCharacter.swift
//  MarvelAPITests
//
//  Created by Nissi Vieira Miranda on 09/11/2019.
//  Copyright © 2019 Nissi Vieira Miranda. All rights reserved.
//

import Foundation

struct ComicCharacter: Decodable {
    let id: Int
    let name: String?
    let description: String?
    let thumbnail: Image?
}
